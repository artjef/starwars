<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Collapsible sidebar using Bootstrap 4</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar" class=" navbar-transparent bg-dark">
            <div class="sidebar-header">
            <img src="image/logo.jpg" alt="..." class="img-thumbnail">
            </div>

            <ul class="list-unstyled components">
               
            <li>
                    <a href="gestion.php">Acceuil</a>
                </li>
                
                <li>
                    <a href="gestionP.php">Personnages</a>
                </li>
                <li>
                    <a href="#">Films</a>
                </li>
                <li>
                    <a href="#">Vaissseaux</a>
                </li>
             
            </ul>

        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-justify"></i>
                    </button>
                   
                        <ul class="nav navbar-nav ml-auto">
                           
                            <li class="nav-item">
                                <a class="nav-link" href="#">admin</a>
                            </li>
                        </ul>
                    </div>
            
            </nav>

            <div class=" contenair-fluid section1 text-center ">
            <div class=" row container-fluid section1 text-center" >
                <div class="col-md-3 col-12  container-fluid mb-4 ">
                  <br>
                  <a href="films_inscrit.php"><h2 class="my-4 mb-5 text-uppercase shadow-lg p-3 text-center film mb-5 bg-white rounded "> <span class="badge badge-white">films </span></h2></a>
                
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                       souhaitez vous apportez des modifications aux films
                      </p>
                  </div>
                  <div class="col-md-3 col-12  container-fluid mb-4 ">
                  <br>
                  <a href="gestionP.php"><h2 class="my-4 mb-5 text-uppercase shadow-lg p-3 text-center film mb-5 bg-white rounded "> <span class="badge badge-white">personnages </span></h2></a>
                
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                    souhaitez vous apportez des modifications aux personnages
                      </p>
                  </div>
                  <div class="col-md-3 col-12  container-fluid mb-4 ">
                  <br>
                  <a href="films_inscrit.php"><h2 class="my-4 mb-5 text-uppercase shadow-lg p-3 text-center film mb-5 bg-white rounded "> <span class="badge badge-white">planètes </span></h2></a>
                
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                    souhaitez vous apportez des modifications aux planètes 
                      </p>
                  </div>
                  <div class="col-md-3 col-12  container-fluid mb-4 ">
                  <br>
                  <a href="films_inscrit.php"><h2 class="my-4 mb-5 text-uppercase shadow-lg p-3 text-center film mb-5 bg-white rounded "> <span class="badge badge-white">vaisseaux </span></h2></a>
                
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                    souhaitez vous apportez des modifications aux vaisseaux
                      </p>
                  </div>
        </div>
            <div class="line"></div>

            <h2>Lorem Ipsum Dolor</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>