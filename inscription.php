<?php  
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale=1">
	<link type="text/css" href="css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="css/bootstrap.js" rel="stylesheet">
	<link href="fontawesome-free-5.15.1-web/css/all.css" rel="stylesheet">
	<link type="text/css" href="css/sign.css" rel="stylesheet">
	<title>Inscription</title>
</head>
<?php include 'navigation.php'; 
$id=0; ?>
    
<body>
	<div class="login-container d-flex align-items-center justify-content-center">
		<form class="inscription-form text-center" action="inscrit.php" method="POST">
			<h1 class="mb-5 font-weight-light text-white text-uppercase">inscription</h1>
			<div class="form-group">
				<input type="text" class="form-control" name="pseudo" id="pseudo" placeholder="pseudo" required>
			</div>
			<div class="form-group">
				<input type="email" class="form-control" name="email" id="email"   placeholder="email" required>
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="mot_de_passe" id="mot_de_passe"  placeholder="mot de passe" required>
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="confirmation" id="confirmation" required placeholder="confirmation du mot de passe">
			</div>
			<button type="submit" name="formsend" id="formsend" class="btn btn-primary btn-block"> inscription</button>
		</form>
	</div>

</body>

<script src="js/jQuery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>


</html>