<?php  include 'BDD.php';
             global $db;
session_start();
$title = "page d'acceuil";

include 'head.php'?>
<body >
    <!--menu de navigation-->
    <?php include 'header.php';
   
    include 'navigation.php'; ?>
    
    <main >
        <div class="contenair-fluid">
            <div id="carouselExampleSlidesOnly"  class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                      <img src="image/fond4.png"  class="d-block w-100" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="image/fond5.png "  class="d-block w-100 " alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="image/fond6.png"  class="d-block w-100" alt="...">
                  </div>
                </div>
              </div>
        </div>
        <div class=" contenair-fluid section1 text-center ">
            <div class=" row container-fluid section1 text-center" >
                <div class="col-md-3 col-12  container-fluid mb-4 ">
                  <br>
                  <a href="films_inscrit.php"><h2 class="my-4 mb-5 text-uppercase shadow-lg p-3 text-center film mb-5 bg-white rounded "> <span class="badge badge-white">films </span></h2></a>
                
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                       aujourd'hui vous avez la possibilité de vivre une expérience incroyable en découvrant chacun des films Star wars 
                      </p>
                  </div>
                  <div class="col-md-3 col-12 my-5  shadow-lg p-3 film mb-5 bg-white rounded  text-center">
                    <img src="image/star_wars9.png" class="container-fluid  ">
                    <a href="ascension_de_skywalker.php"><h4 class="my-4 font-weight-bold text-center">star wars: <br>l'ascension de skywalker</h4></a>
                  </div>
              <div class="col-md-3  col-12 my-5  shadow-lg p-3 mb-5 film bg-white rounded text-center">
              <a href="dernier_des_jedi.php"> <img src="image/stra_wars8.png" class="container-fluid"></a>
                <a href="dernier_des_jedi.php"><h4 class="my-4 font-weight-bold text-center">star wars: <br>le dernier des jedi</h4></a>
              </div>
              <div class="col-md-3 col-12 my-5 shadow-lg p-3 mb-5 film bg-white rounded text-center">
              <a href="dernier_des_jedi.php"><img src="image/Star_Wars7.png" class="container-fluid "></a>
                <a href="reveil_de_la_force.php"><h4 class="my-4 font-weight-bold text-center">star wars:<br> le reveil de la force</h4>
              </div>
                
            </div>
            <div class="row my-2 container-fluid text-center">
              <div class="col-md-12 col-sm-12">
                 <a href="films_inscrit.php"> <h4 class=" text-uppercase text-dark suite"><u> voir plus </u></h4></a>
              </div>
          </div>
        </div>
          <section class=" contenair-fluid section2 text-center">
            <div class=" row container-fluid " >
                <div class="col-md-3 col-12   mb-4 ">
                  <br>
                    <a href="personnages.php"><h2 class="my-4 text-uppercase shadow-lg p-3 film bg-white rounded titres"> <span class="badge badge-white">personnages </span></h2></a>
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                        
                       Apprenez à connaitre les personnages qui ont marqués l'histoire de chacun des films Star wars </p>
                  </div>
                  <a href="dark_vador.php"><div class="col-md-3 col-12 my-5  film shadow-lg p-3 mb-5 bg-white rounded  text-center">
                   <img src="image/stickers-geant-dark-vador-star-wars-ref-22584.jpg" class="container-fluid">
                    <a href="dark_vador.php"><h4 class="my-4 font-weight-bold text-center">Dark Vador</h4></a>
                  </div></a>
              <div class="col-md-3  col-12 my-5  film shadow-lg p-3 mb-5 bg-white rounded  text-center">
              <a href="anakin.php"> <img src="image/Anakin_Skywalker.png" class="container-fluid"></a>
                <a href="anakin.php"> <h4 class="my-4 font-weight-bold text-center">Anakin Skywalker</h4></a>
              </div>
              <div class="col-md-3 col-12 my-5 film shadow-lg p-3 mb-5 bg-white rounded text-center">
              <a href="yoda.php"><img src="image/Yoda.png" class="container-fluid "></a>
                <a href="yoda.php"><h4 class="my-4 font-weight-bold text-center">Maitre yoda</h4></a>
              </div>
                
            </div>
            <div class="row my-2 container-fluid ">
              <div class="col-md-12 col-sm-12">
              <a href="personnages.php"><h4 class="text-uppercase text-dark suite"><u> voir plus </u></h4></a>
              </div>
          </div>
          </section>
          <section class=" contenair-fluid section3 text-center">
            <div class=" row container-fluid " >
                <div class="col-md-3 col-12 text-center  mb-4 ">
                <a href="planetes_admin.php"> <h2 class="my-4 mb-5 text-uppercase titres shadow-lg film p-3 mb-5 bg-white rounded"> <span class="badge badge-white">planètes </span></h2></a>
                    <p  class=" text-center text-uppercase  text-black commentaires"> 
                        
                       Venez découvrir les planètes qui ont été présentes dans chacun des films Star wars </p>
                  </div>
                  <div class="col-md-3 col-12 my-5 film shadow-lg p-3 mb-5 bg-white rounded  text-center">
                  <a href="aenten2.php"> <img src="image/Aeten II.png" class="container-fluid"> </a>
                    <a href="aenten2.php"> <h4 class="my-4 font-weight-bold text-center">Aenten II</h4></a>
                  </div>
              <div class="col-md-3  col-12 my-5 film shadow-lg p-3 mb-5 bg-white rounded  text-center">
              <a href="agamar.php"><img src="image/Agamar.png" class="container-fluid"></a>
                <a href="agamar.php"><h4 class="my-4 font-weight-bold text-center">Agamar</h4></a>
              </div>
              <div class="col-md-3 col-12 my-5  film shadow-lg p-3 mb-5 bg-white rounded text-center">
              <a href="allyuen.php"> <img src="image/Allyuen.png" class="container-fluid "></a>
                <a href="allyuen.php"> <h4 class="my-4 font-weight-bold text-center">Allyuen</h4></a>
              </div>
                
            </div>
            <div class="row my-2 container-fluid ">
              <div class="col-md-12 col-sm-12">
                  <a href="planetes_admin.php" ><h4 class=" text-uppercase text-center text-dark suite "><u> voir plus </u></h4></a>
              </div>
          </div>
          </section>
          <section class=" contenair-fluid section4 text-center">
            <div class=" row container-fluid " >
                <div class="col-md-3 col-12 text-center  mb-4 ">
              <a  href="vaisseaux_admin.php">   <h2 class="my-4 mb-5 text-uppercase titres shadow-lg film p-3 mb-5 bg-white rounded"> <span class="badge badge-white">vaisseaux </span></h2></a>
              <a  href="vaisseaux_admin.php">  <p  class=" text-center text-uppercase  text-black commentaires"> </a>
                        
                       Venez explorer les incroyables vaisseaux qui ont marqué l'univers Star wars </p>
                  </div>
                  <div class="col-md-3 col-12 my-5 film shadow-lg p-3 mb-5 bg-white rounded  text-center">
                  <a href="b-wing.php">  <img src="image/B-Wing (1).png" class="container-fluid"></a>
                  <a href="b-wing.php">  <h4 class="my-4 font-weight-bold text-center">B-Wing</h4></a>
                  </div>
              <div class="col-md-3  col-12 my-5  film shadow-lg p-3 mb-5 bg-white rounded  text-center">
              <a href="venator.php">   <img src="image/Venator.png" class="container-fluid"></a>
              <a href="venator.php">    <h4 class="my-4 font-weight-bold text-center">Venator</h4></a>
              </div>
              <div class="col-md-3 col-12 my-5  film shadow-lg p-3 mb-5 bg-white rounded text-center">
              <a href="nebulon.php">    <img src="image/Nebulon-B.png" class="container-fluid "></a>
              <a href="nebulon.php">    <h4 class="my-4 font-weight-bold text-center">Nebulon-B</h4></a>
              </div>
               
            </div>
            <div class="row my-2 container-fluid  ">
              <div class="col-md-12 col-sm-12">
              <a href="vaisseaux_admin.php">     <h4 class="text-uppercase text-dark suite text-center" ><u> voir plus </u></h4></a>
              </div>
          </div>
          </section>

    </main>
    <footer class="bg-dark text-center">
      <p class="text-uppercase text-white fin">
        &copy;
        <br>
      PRISO Jeffrey
      <br>
      Arthur SANDJONG</p>
      
    </footer>
<script src="js/jQuery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>